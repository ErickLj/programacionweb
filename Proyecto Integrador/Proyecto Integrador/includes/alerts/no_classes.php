<div class="mt-3 px-5">
<div class='alert alert-warning mt-4 text-center' role='alert'>
<i class="bi bi-exclamation-triangle"></i> No tienes materias registradas! <a class='fw-bold text-body' href="../../dashboard/materias/">Ir a Materias!</a>
</div>
</div>