<label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Rol</label>
<select required class="form-select mb-2" name="rol" id="rol">
    <option value="1" <?php if($user['rol']=="1") {echo "selected";}?>>Estudiante</option>
    <option value="2" <?php if($user['rol']=="2") {echo "selected";}?>>Administrador</option>
</select >