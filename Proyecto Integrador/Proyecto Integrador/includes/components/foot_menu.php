<footer class="bg-dark shadow-lg text-white pt-5  text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-3">
                <h5>Contacto</h5>
                <ul class="list-unstyled">
                    <li>Tgo. en Desarrollo de Software: Erick López Jáuregui</li>
                    <li>Correo: erick2001.lopez@gmail.com</li>
                    <li>Teléfono: +52 33 1228 68 07</li>
                </ul>
            </div>
            <div class="col-md-3 mb-3">
                <h5>Navegación</h5>
                <ul class="list-unstyled">
                    <li><a href="#" class="text-white">Volver al Inicio</a></li>
                    <li><a href="../../" class="text-white">Menú principal</a></li>
                </ul>
            </div>
            <div class="col-md-3 mb-3">
                <h5>Sociales</h5>
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="https://gitlab.com/ErickLj" class="text-white" target="_blank"><i class="bi bi-gitlab"></i> Repositorio de GitLab</a></li>
                    <li class="list-inline-item"><a href="linkedin.com/in/erick-arturo-lópez-jáuregui-568176227" class="text-white" target="_blank"><i class="bi bi-linkedin"></i> LinkedIn</a></li>
                </ul>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-12 text-center">
                <p>Licenciatura en Tecnologías de la Información. UNIVERSIDAD DE GUADALAJARA</p>
            </div>
        </div>
    </div>
</footer>
