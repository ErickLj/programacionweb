<?php
    // Definir el color de la tarjeta
    
    include_once '../../config/functions.php';

    $cardColor = cardColor($tmp['prioridad']);
    $datecolor = dueColor($tmp['fecha_vencimiento']);

?>

<div class="card border-<?=$cardColor?> my-3 shadow">
    <div class="card-header bg-<?=$cardColor?> text-white"><?= $tmp['materia_nombre']?></div>
    <div class="card-body text-secondary">
        <h6 class="mb-2">
            <span class="badge bg-<?=$datecolor?>"><i class="bi bi-clock"></i> <?= $tmp['fecha']?></span>
        </h6>
        <p class="card-title"><?= $tmp['titulo']?></p>
        
    </div>
    <div class="card-footer bg-white">
        <button type="button" class="btn btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#modalOpen<?=$tmp['id']?>">Abrir</button>
        <?php
        if($tmp['estado']!=3){include '../../dashboard/tareas/tarea_completar.php';}
        include '../../dashboard/tareas/tarea_eliminar.php';
        ?>
    </div> 
</div>

<div class="modal fade" id="modalOpen<?=$tmp['id']?>" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel"><?=$tmp['titulo']?></h1><br>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-header">
            <h5  class="mb-2">
                <span class="mb-1 badge bg-<?=$datecolor?>"><i class="bi bi-clock"></i> Vence: <?= $tmp['fecha']?></span>
                <span class="mb-1 badge bg-<?=$cardColor?> gradient-custom-1">Prioridad: <?= $tmp['prioridad_nombre']?></span>
                <span class="mb-1 badge bg-<?=$cardColor?> gradient-custom-2"><i class="bi bi-book"></i> <?= $tmp['materia_nombre']?></span>
            </h5>
            </div>
            
            <div class="modal-body">
                <?= $tmp['descripcion']?>
            </div>
            <div class="modal-footer justify-content-center">
                <a type="button" class="btn btn-outline-info"  href="../../dashboard/tareas/tarea_editar.php?id=<?=$tmp['id']; ?>">Editar <i class="bi bi-pencil-fill"></i></a>
            </div>
        </div>
    </div>
</div>  