
<div class="sidebar border-black col-xs-12 col-sm-12 col-md-3 col-lg-2 p-0 bg-dark ">
    <a href="./../" class="d-flex align-items-center justify-content-center m-3 nav-link link-body-emphasis">
        <span class="fs-5 text-white p-2"><i class="bi bi-house-door-fill"></i> Bienvenido <?= $_SESSION['user'] ?></span>
    </a>
    <hr>
    <ul class="nav nav-pills flex-column mb-auto align-items-center">
        <li>
            <a href="../../dashboard/tareas/" class="nav-link link-body-emphasis text-white fs-4 mb-3"><i class="bi bi-list-task"></i> Tareas</a>
        </li>
        <li>
            <a href="../../dashboard/materias/" class="nav-link link-body-emphasis text-white fs-5 mb-3"><i class="bi bi-book-fill"></i> Materias</a>
        </li>
        <?php if($_SESSION['rol'] == 2) { ?>                      
        <li>
            <a href="../../dashboard/usuarios/" class="nav-link link-body-emphasis text-white fs-5 mb-3"><i class="bi bi-people-fill"></i> Usuarios</a>
        </li>
        <?php } ?>
    </ul>
    <hr>
    <div class="dropdown m-3">
        <a href="#" class="d-flex align-items-center justify-content-center m-3 nav-link link-body-emphasis dropdown-toggle text-white" data-bs-toggle="dropdown" aria-expanded="false">
        <i class="bi bi-person-circle fs-4"></i><strong class="fs-5"> <?=$_SESSION['user'] ?></strong> 
        </a>
        <ul class="dropdown-menu text-small shadow bg-dark">
            <li><a class="dropdown-item bg-dark text-white" href="../../dashboard/usuarios/usuario_editar.php?id=<?=$_SESSION['id']?>">Editar Perfil</a></li>
            <li><a class="dropdown-item bg-dark text-white" href="../../config/controlers/salir.php">Cerrar Sesión</a></li>
        </ul>
    </div>
</div>
