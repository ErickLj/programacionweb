<nav class="navbar navbar-dark bg-dark sticky-top text-center border-bottom border-dark shadow-lg ">
    <div class="container-fluid">
        <a class="navbar-brand mb-0 h1" href="./">
            <i class="bi bi-folder" ></i> Taskerp
        </a>
        
        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="offcanvas offcanvas-end text-bg-dark" tabindex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
            <div class="offcanvas-header">
                <h5>Bienvenido <?= $_SESSION['user'] ?></h5>
                <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <ul class="navbar-nav justify-content-end flex-grow-1 pe-3 pb-3">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="./">Volver al Inicio</a>
                    </li>
                </ul>
                <div class="d-grid gap-3">
                    <a class="btn btn-outline-danger" href="../../config/controlers/salir.php"><i class="bi bi-box-arrow-right"></i> Cerrar Sesión</a>
                    <a class="btn btn-outline-primary" href="../../../"><i class="bi bi-folder-fill"></i> Ir a Portafolio del Curso</a>
                    <a class="btn btn-outline-light" href="https://gitlab.com/ErickLj" role="button" target="_blank"><i class="bi bi-gitlab"></i> @ErickLj</a>
                    <a class="btn btn-outline-info" href="linkedin.com/in/erick-arturo-lópez-jáuregui-568176227" role="button" target="_blank"><i class="bi bi-linkedin"></i> LinkedIn</a>
                </div>
            </div>
        </div>
    </div>
</nav>