<?php require '../../config/connections/admin_validate.php'; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Usuarios</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
        <link rel="stylesheet" href="../../assets/css/style.css">
        <link rel="icon" href="../../assets/ico/logo.ico">
    </head>
    <body>
        <?php include '../../includes/components/header.php'; ?>
        <div class="container-fluid">
            <div class="row">
                <?php include '../../includes/components/sidebar.php'; ?>
                    <div class="col-lg-10 col-md-9 col-sm-12 p-0 screen-height bg-light">
                        <div class="container py-5 h-100">
                            <div class="row d-flex justify-content-center align-items-center h-100">
                            <div class="col-md-12 col-xl-10 ">
                                <div class="card mb-5">
                                    <div class="card-body p-4 ">
                                        <div class="pt-3 pb-2 table-responsive">
                                        
                                            <h2 class="mb-2">Lista de Usuarios</h2>
                                        <?php
                                        if(isset($_SESSION['msg'])){
                                            switch($_SESSION['msg']) {

                                                case 1:
                                                    include '../../includes/alerts/register_duplicated.php';
                                                    unset($_SESSION['msg']);
                                                    break;

                                                case 2:
                                                    include '../../includes/alerts/success/create_user_success.php';
                                                    unset($_SESSION['msg']);
                                                    break;  

                                                case 3:
                                                    include '../../includes/alerts/success/update_user_success.php';
                                                    unset($_SESSION['msg']);
                                                    break;

                                                case 4:
                                                    include '../../includes/alerts/form_empty.php';
                                                    unset($_SESSION['msg']);
                                                    break;

                                                case 5:
                                                    include '../../includes/alerts/success/delete_user_success.php';
                                                    unset($_SESSION['msg']);
                                                    break;
                                                case 10:
                                                    include '../../includes/alerts/success/update_user_success.php';
                                                    unset($_SESSION['msg']);
                                                    break;
                                                default:
                                                break;
                                            }
                                        } 
                                        include 'usuario_insertar.php';
                                        ?>

                                        <?php
                                            require '../../config/connections/db_connection.php';
                                            $user_list_sql = "SELECT * FROM usuarios /*WHERE id != 2 */ORDER BY id";
                                            $user_list_sql = $conn->query($user_list_sql);

                                            if($user_list_sql->rowCount() > 0) {

                                                require '../../config/functions.php';
                                        ?>

                                        <table class="table text-white mb-0 ">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Usuario</th>
                                                    <th scope="col">Nombre</th>
                                                    <th scope="col">Apellido</th>
                                                    <th scope="col" >Correo</th>
                                                    <th scope="col" >Rol</th>
                                                    <th scope="col" >Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($user_list_sql as $tmp) { ?> 
                                                <tr class="fw-normal">
                                                    <th class="align-middle"><span class="ms-2"><?= $tmp['usuario']?></span></th>
                                                    <td class="align-middle"><span><?= $tmp['nombre']?></span></td>
                                                    <td class="align-middle"><span><?= $tmp['apellido']?></span></td>
                                                    <td class="align-middle"><span><?= $tmp['correo']?></span> </td>
                                                    <td class="align-middle"><h6 class="mb-0"><span class="badge bg-success <?=isMe($tmp['id'])?>"><?= validateRol($tmp['rol'])?> </span></h6></td>
                                                    <td class="align-middle">
                                                        <a class="btn btn-outline-success m-1" href="usuario_editar.php?id=<?=$tmp['id']?>"><i class="bi bi-pencil-fill"></i></a>
                                                        <?php include 'usuario_eliminar.php';?>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        </div>
                                        <?php
                                            } else {
                                                echo "No hay usuarios registrados!";
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                    </div>
            </div>
        </div>
        <?php include '../../includes/components/footer.php'; ?>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    </body>
</html>