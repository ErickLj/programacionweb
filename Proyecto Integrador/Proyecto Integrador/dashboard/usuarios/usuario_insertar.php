<div class="my-4">
    <button type="button" class="btn  btn-lg form-control" data-bs-toggle="collapse" data-bs-target="#newuser">
        <i class="bi bi-plus-circle"></i> Nuevo usuario
    </button>
    <?php
    if(isset($_SESSION['msg'])){
        switch($_SESSION['msg']) {

            case 1:
                include '../../includes/alerts/register_duplicated.php';
                unset($_SESSION['msg']);
                break;

            case 2:
                include '../../includes/alerts/success/create_user_success.php';
                unset($_SESSION['msg']);
                break;  

            case 3:
                include '../../includes/alerts/success/update_user_success.php';
                unset($_SESSION['msg']);
                break;

            case 4:
                include '../../includes/alerts/form_empty.php';
                unset($_SESSION['msg']);
                break;

            case 5:
                include '../../includes/alerts/success/delete_user_success.php';
                unset($_SESSION['msg']);
                break;
            case 10:
                include '../../includes/alerts/success/update_user_success.php';
                unset($_SESSION['msg']);
                break;
            default:
            break;
        }
    } 
    ?>
</div>
<div class="collapse" id="newuser">
    <div class="card card-body my-3">
        <form action="../../config/controlers/usuario.insertar.php" method="POST"> 
            <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Nombres</label>
            <input required type="text" name="nombre" id="nombre" class="form-control form-control-lg col-lg-3 col-12 mb-3" placeholder="Ej. Juan">
            <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Apellidos</label>
            <input required type="text" name="apellido" id="apellido" class="form-control form-control-lg col-lg-3 col-12 mb-3" placeholder="Ej. Hernandez">
            <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Rol</label>
            <select  class="form-select col-lg-12 col-12 mb-3" name="rol" id="rol">
                <option selected value="1">Estudiante</option>
                <option value="2">Administrador</option>
            </select >
            <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Nombre de Usuario</label>
            <input required type="text" name="usuario" id="usuario" class="form-control form-control-lg col-lg-3 col-12 mb-3" placeholder="Juan2024" />
            <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Correo</label>
            <input required type="email" placeholder="@ejemplo.com" name="correo" id="correo" class="form-control form-control-lg col-lg-3 col-12 mb-3"/>
            <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Contraseña</label>
            <input required type="password" placeholder="No la olvides!" name="contrasena" id="contrasena" class="form-control form-control-lg col-lg-3 col-12 mb-3" />
            <input class="btn btn-outline-success gradient-custom-3 form-control mb-3" type="submit" value="Registrar">
            <div class="d-flex justify-content-center">
            </div>
        </form>
    </div>
</div>