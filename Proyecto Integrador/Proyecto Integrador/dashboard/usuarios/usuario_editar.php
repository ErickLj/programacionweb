<?php
    session_start();
    if(!isset($_SESSION['user']) || $_SESSION['rol'] != 2){
        // Función de js que regresa a la ventana anterior
        if($_GET['id'] != $_SESSION['id']) {
            echo '<script>window.history.go(-1);</script>';
            exit;
        }
    }

?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Editar Usuario</title>
        <link rel="stylesheet" href="../../assets/css/style.css">
        <link rel="icon" href="../../assets/ico/logo.ico">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    </head>
    <body>
    <?php include '../../includes/components/header.php'; ?>
            <div class="container-fluid">
                <div class="row">
                    <?php include '../../includes/components/sidebar.php'; ?>
                        <div class="col-lg-10 col-sm-12 screen-height bg-light">
                                <div class="align-items-center h-100 ">
                                    <div class="container h-100">
                                        <div class="row d-flex justify-content-center align-items-center h-100">
                                            <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                                                <div class="card" style="border-radius: 15px;">
                                                    <div class="card-body p-5">
                                                        <h3 class="text-center mb-5">Editar usuario</h3>

                                                        <?php
                                                            $user_id = $_GET['id'];
                                                            require '../../config/connections/db_connection.php';
                                                                                                                                                        
                                                            $user_edit_sql = "SELECT * FROM usuarios WHERE id = :id";
                                                            $user_edit_sql = $conn->prepare($user_edit_sql);
                                                            $user_edit_sql->bindParam(':id', $user_id);
                                                            $user_edit_sql->execute();
                                                            if($user_edit_sql->rowCount() > 0) {

                                                                $user = $user_edit_sql->fetch(PDO::FETCH_ASSOC);
                                                            ?>
                                                                <form action="../../config/controlers/usuario.editar.php" method="POST"> 

                                                                    <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Nombre/s</label>
                                                                    <input required type="text" name="nombre" id="nombre" class="form-control form-control-lg mb-2" value="<?=$user['nombre']?>">
                                                                    
                                                                    <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Apellido/s</label>
                                                                    <input required type="text" name="apellido" id="apellido" class="form-control form-control-lg mb-2" value="<?=$user['apellido']?>">

                                                                    <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Nombre de Usuario</label>
                                                                    <input required type="text" name="usuario" id="usuario" class="form-control form-control-lg mb-2" value="<?=$user['usuario']?>"/>
                                                                    
                                                                    <?php
                                                                    if($_SESSION['rol'] == 2) {
                                                                        include '../../includes/components/user_rol.php';
                                                                    } else {
                                                                        echo "<input type='hidden' id='rol' name='rol' value='1'>";
                                                                    }
                                                                    ?>

                                                                    <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Correo</label>
                                                                    <input required type="email" placeholder="Correo" name="correo" id="correo" class="form-control form-control-lg mb-2" value="<?=$user['correo']?>"/>
                                                                    
                                                                    <input type="hidden" id="id" name="id" value="<?=$user['id']?>">

                                                                    <input class="btn btn-outline-success btn-lg mt-3 form-control" type="submit" value="Guardar Cambios">
                                                                </form>
                                                                    
                                                                <?php
                                                                
                                                            } 

                                                            if(isset($_SESSION['msg'])){
                                                                
                                                                switch($_SESSION['msg']) {
                                                                    
                                                                    case 1:
                                                                        include '../../includes/alerts/register_duplicated.php';
                                                                        unset($_SESSION['msg']);
                                                                        break;
                                                                        
                                                                    case 2:
                                                                        include '../../includes/alerts/form_empty.php';
                                                                        unset($_SESSION['msg']);
                                                                        break;
                                                                    case 10:
                                                                        include '../../includes/alerts/success/update_user_success.php';
                                                                        unset($_SESSION['msg']);
                                                                        break;
                                                                    default:
                                                                        unset($_SESSION['msg']);
                                                                    break;
                                                                }
                                                            }

                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include '../../includes/components/footer.php'; ?>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="assets/css/style.css">
    </body>
    </html>