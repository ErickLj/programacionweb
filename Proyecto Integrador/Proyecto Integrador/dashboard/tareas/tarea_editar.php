<?php 
    require '../../config/connections/session_validate.php'; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Tarea</title>
    <link rel="stylesheet" href="../../assets/css/style.css">
    <link rel="icon" href="../../assets/ico/logo.ico">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<?php include '../../includes/components/header.php'; ?>
        <div class="container-fluid">
            <div class="row">
                <?php include '../../includes/components/sidebar.php'; ?>
                    <div class="col-lg-10 col-sm-12 screen-height px-0">
                            <div class="align-items-center h-100 bg-light">
                                <div class="container h-100">
                                    <div class="row d-flex justify-content-center align-items-center h-100">
                                        <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                                            <div class="card" style="border-radius: 15px;">
                                                <div class="card-body p-5">
                                                    <h3 class="text-center mb-5">Editar Tarea</h3>

                                                    <?php
                                                        $task_id = $_GET['id'];
                                                        require '../../config/connections/db_connection.php';
                                                                                                                                                    
                                                        $task_edit_sql = "SELECT * FROM tareas WHERE id = :id";
                                                        $task_edit_sql = $conn->prepare($task_edit_sql);
                                                        $task_edit_sql->bindParam(':id', $task_id);
                                                        $task_edit_sql->execute();

                                                        $class_list_sql = "SELECT nombre, id FROM materias WHERE usuario = '" . $_SESSION['id'] . "'";
                                                        $class_list_sql = $conn->query($class_list_sql);  

                                                        if($task_edit_sql->rowCount() > 0) {

                                                            $task = $task_edit_sql->fetch(PDO::FETCH_ASSOC);
                                                        ?>
                                                            <form action="../../config/controlers/tarea.editar.php" method="POST">
                                                                <div class="mb-3">
                                                                    <input required type="text" name="titulo" id="titulo" class="form-control" placeholder="Título" value="<?=$task['titulo']?>">
                                                                </div>
                                                                <div class="mb-3">
                                                                    <textarea required class="form-control" name="descripcion" id="descripcion" rows="4" placeholder="Describe tu tarea aquí..."><?=$task['descripcion']?></textarea>
                                                                </div>
                                                                <label class="form-label mb-1">Prioridad</label><br>
                                                                <div class="form-check form-check-inline mb-3">
                                                                    <input required class="form-check-input" type="radio" name="prioridad" id="prioridad3" value="3" <?php if($task['prioridad']=="3") {echo "checked";}?>>
                                                                    <label required class="form-check-label" for="inlineRadio1">Baja</label>
                                                                </div>
                                                                <div class="form-check form-check-inline mb-3">
                                                                    <input required class="form-check-input" type="radio" name="prioridad" id="prioridad2" value="2" <?php if($task['prioridad']=="2") {echo "checked";}?>>
                                                                    <label class="form-check-label" for="inlineRadio2">Media</label>
                                                                </div>
                                                                <div class="form-check form-check-inline mb-3">
                                                                    <input required class="form-check-input" type="radio" name="prioridad" id="prioridad1" value="1" <?php if($task['prioridad']=="1") {echo "checked";}?>>
                                                                    <label class="form-check-label" for="inlineRadio3">Alta</label>
                                                                </div>
                                                                <select required class="form-select mb-3" name="materia" id="materia">
                                                                    <option value="<?=$task['materia']?>" selected>Mantener materia anterior</option>
                                                                    <?php
                                                                        foreach($class_list_sql as $tmp) { 
                                                                            echo "<option  value='". $tmp['id'] ."'>  ". $tmp['nombre'] ."</option>";
                                                                        }
                                                                    ?>
                                                                </select>                      
                                                                <label class="form-label mb-1">Fecha de Vencimiento</label>
                                                                
                                                                <input required id="fecha_vencimiento" name="fecha_vencimiento" class="form-control mb-3" type="datetime-local" value="<?=$task['fecha_vencimiento']?>"/>
                                                                <label class="form-label mb-1">Estado</label><br>
                                                                <div class="form-check form-check-inline mb-3">
                                                                    <input required class="form-check-input" type="radio" name="estado" id="estado1" value="1" <?php if($task['estado']=="1") {echo "checked";}?>>
                                                                    <label class="form-check-label" for="inlineRadio1">Por hacer</label>
                                                                </div>
                                                                <div class="form-check form-check-inline mb-3">
                                                                    <input required class="form-check-input" type="radio" name="estado" id="estado2" value="2" <?php if($task['estado']=="2") {echo "checked";}?>>
                                                                    <label class="form-check-label" for="inlineRadio2">Pendiente</label>
                                                                </div>
                                                                <div class="form-check form-check-inline mb-3">
                                                                    <input required class="form-check-input" type="radio" name="estado" id="estado3" value="3" <?php if($task['estado']=="3") {echo "checked";}?>>
                                                                    <label class="form-check-label" for="inlineRadio3">Completada</label>
                                                                </div>
                                                                <input type="hidden" id="id" name="id" value="<?=$task['id']?>">
                                                                <div class="input-group mb-3">
                                                                    <button type="submit" class="btn btn-primary btn-block form-control">Guardar cambios</button>
                                                                </div>
                                                            </form>
                                                                
                                                            <?php
                                                            
                                                        } else {
                                                            echo "Esa materia no fue encontrada! ";
                                                        }
                                                        if(isset($_SESSION['msg'])){
                                                            
                                                            switch($_SESSION['msg']) {
                
                                                                case 2:
                                                                    include '../../includes/alerts/form_empty.php';
                                                                    unset($_SESSION['msg']);
                                                                    break;
                                                                case 10:
                                                                    include '../../includes/alerts/success/update_user_success.php';
                                                                     unset($_SESSION['msg']);
                                                                    break;

                                                            }
                                                        }

                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include '../../includes/components/footer.php'; ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="assets/css/style.css">
</body>
</html>