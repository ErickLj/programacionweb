<?php require '../../config/connections/session_validate.php';

require_once('../../config/functions.php');


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tareas</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
        <link rel="stylesheet" href="../../assets/css/style.css">
        <link rel="icon" href="../../assets/ico/logo.ico">
    </head>
    <body>
        <?php include '../../includes/components/header.php'; ?>
        <div class="container-fluid ">
            <div class="row">
                <?php include '../../includes/components/sidebar.php'; ?>
                <div class="col-lg-10 col-md-9 col-sm-12 p-0 screen-height bg-light">
                    <div class="row row-height container-fluid p-0 m-0">
                        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 px-5">
                            <?php 
                                require '../../config/connections/db_connection.php';     
                                                
                                $class_list_sql = "SELECT nombre, id FROM materias WHERE usuario = '" . $_SESSION['id'] . "'";
                                $class_list_sql = $conn->query($class_list_sql);  

                                if($class_list_sql->rowCount() > 0) { 
                                    
                                    require 'tarea_filtrar.php';

                                } else {
                                    include '../../includes/alerts/no_classes.php';
                                }
                            ?>
                            
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 px-5">
                            <?php 
                            
                                if($class_list_sql->rowCount() > 0) { 
                                    include 'tarea_insertar.php';
                                } 
                            ?>
                            
                        </div>
                    </div>
                
                    <div class="row row-height container-fluid p-0 m-0">
                    <?php   

                            if(isset($_GET['id']) && !empty($_GET['id'])){
                                $index_search = "AND materias.id = " .$_GET['id'] . " ";
                            } else {
                                $index_search = "AND 1=1";
                            }   
                    
                            $task_list_sql = 
                                "SELECT tareas.*,
                                DATE_FORMAT(tareas.fecha_vencimiento,'%e %b %l:%i %p') AS fecha,
                                materias.nombre AS materia_nombre,
                                materias.id AS materia_id,
                                prioridades.tipo AS prioridad_nombre
                                FROM tareas 
                                INNER JOIN materias 
                                    ON tareas.materia = materias.id 
                                INNER JOIN prioridades     
                                    ON tareas.prioridad = prioridades.id 
                                WHERE tareas.usuario = " . $_SESSION['id'] ." ". $index_search . "  ORDER BY tareas.id";
                                
                            $task_list_sql = $conn->query($task_list_sql);

                            //preparar consulta


                            $registros_estado1 = [];
                            $registros_estado2 = [];
                            $registros_estado3 = [];

                            if($task_list_sql->rowCount() > 0) { 
                                
                                foreach($task_list_sql as $tmp) { 

                                    switch ($tmp['estado']) {
                                        case 1:
                                            $registros_estado1[] = $tmp;
                                            break;
                                        case 2:
                                            $registros_estado2[] = $tmp;
                                            break;
                                        case 3:
                                            $registros_estado3[] = $tmp;
                                            break;
                                    }
                                }
                            } 
                            ?>

                        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 px-5">
                            
                            <h2 class="form-control mt-3 fs-4 text-center text-secondary fw-bold">TAREAS POR HACER</h2>
                            
                            <?php
                            foreach($registros_estado1 as $tmp) { 
                                    include '../../includes/components/modal_task_card.php';
                            } ?>
                        </div>

                        <div class="col-xl-4  col-lg-6 ccol-md-12 col-sm-12 px-5 ">
                            <h2 class="form-control mt-3 fs-4 text-center text-secondary fw-bold">TAREAS PENDIENTES</h5>
                            <?php
                            foreach($registros_estado2 as $tmp) { 
                                    include '../../includes/components/modal_task_card.php';
                            } ?>
                        </div>

                        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 px-5">
                            <h2 class="form-control mt-3 fs-4 text-center text-secondary fw-bold">TAREAS COMPLETADAS</h5>
                            <?php
                            foreach($registros_estado3 as $tmp) { 
                                    include '../../includes/components/modal_task_card.php';
                            } ?>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <?php include '../../includes/components/footer.php'; ?>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    </body>
</html>