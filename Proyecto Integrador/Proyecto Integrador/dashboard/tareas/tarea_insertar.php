<div class="mt-3">
    <button type="button" class="btn  btn-lg form-control" data-bs-toggle="collapse" data-bs-target="#newtask">
        <i class="bi bi-plus-circle"></i> Nueva tarea
    </button>
    <?php
    if(isset($_SESSION['msg'])){
        switch($_SESSION['msg']) {
            case 1:
                include '../../includes/alerts/success/task_success.php';
                unset($_SESSION['msg']);
                break;
            case 2:
                include '../../includes/alerts/form_empty.php';
                unset($_SESSION['msg']);
                break;
            case 3:
                include '../../includes/alerts/success/update_task_success.php';
                unset($_SESSION['msg']);
                break;
            case 4:
                include '../../includes/alerts/success/update_user_success.php';
                unset($_SESSION['msg']);
                break;
            case 10:
                
                unset($_SESSION['msg']);
                break;
        }
    } 
    ?>
</div>
<div class="collapse" id="newtask">
    <div class="card card-body mt-3">
        <form action="../../config/controlers/tarea.insertar.php" method="POST">
            <div class="mb-3">
                <input required type="text" name="titulo" id="titulo" class="form-control" placeholder="Título">
            </div>
            <div class="mb-3">
                <textarea required  class="form-control" name="descripcion" id="descripcion" rows="4" placeholder="Describe tu tarea aquí..."></textarea>
            </div>
            <label class="form-label mb-1">Prioridad</label><br>
            <div class="form-check form-check-inline mb-3">
                <input required class="form-check-input" type="radio" name="prioridad" id="prioridad3" value="3">
                <label class="form-check-label" for="inlineRadio1">Baja</label>
            </div>
            <div class="form-check form-check-inline mb-3">
                <input required class="form-check-input" type="radio" name="prioridad" id="prioridad2" value="2">
                <label class="form-check-label" for="inlineRadio2">Media</label>
            </div>
            <div class="form-check form-check-inline mb-3">
                <input required class="form-check-input" type="radio" name="prioridad" id="prioridad1" value="1">
                <label class="form-check-label" for="inlineRadio3">Alta</label>
            </div>
            <select required class="form-select mb-3" name="materia" id="materia">
                <option value="" selected>Selecciona una Materia</option>
                <?php
                    require '../../config/connections/db_connection.php';     
                                            
                    $class_list_sql = "SELECT nombre, id FROM materias WHERE usuario = '" . $_SESSION['id'] . "'";
                    $class_list_sql = $conn->query($class_list_sql);  
                    
                    foreach($class_list_sql as $tmp) { 
                        echo "<option  value='". $tmp['id'] ."'>  ". $tmp['nombre'] ."</option>";
                    }
                ?>
            </select>                      
            <label class="form-label mb-1">Fecha de Vencimiento</label>
            <input required id="fecha_vencimiento" name="fecha_vencimiento" class="form-control mb-3" type="datetime-local" />
            <label class="form-label mb-1">Estado</label><br>
            <div required class="form-check form-check-inline mb-3">
                <input required class="form-check-input" type="radio" name="estado" id="estado1" value="1">
                <label class="form-check-label" for="inlineRadio1">Por hacer</label>
            </div>
            <div class="form-check form-check-inline mb-3">
                <input required class="form-check-input" type="radio" name="estado" id="estado2" value="2">
                <label class="form-check-label" for="inlineRadio2">Pendiente</label>
            </div>
            <div class="form-check form-check-inline mb-3">
                <input required class="form-check-input" type="radio" name="estado" id="estado3" value="3">
                <label class="form-check-label" for="inlineRadio3">Completada</label>
            </div>
            <div class="input-group mb-3">
                <button type="submit" class="btn btn-outline-success btn-block form-control">Guardar</button>
            </div>
        </form>
    </div>
</div>