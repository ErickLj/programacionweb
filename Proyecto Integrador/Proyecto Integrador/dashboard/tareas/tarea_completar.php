<button type="button" class="btn btn-outline-success " data-bs-toggle="modal" data-bs-target="#modalFinished<?=$tmp['id']?>">
<i class="bi bi-check-square-fill"></i>
</button>  

<div class="modal fade" id="modalFinished<?=$tmp['id']?>" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel">Marcar tarea como completada?</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-footer justify-content-center">
                <a type="button" class="btn btn-success"  href="../../config/controlers/tarea.completar.php?id=<?=$tmp['id']; ?>">Completar</a>
            </div>
        </div>
    </div>
</div>   