<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrar Usuario</title>
    <link rel="stylesheet" href="../../assets/css/style.css">
    <link rel="icon" href="../assets/ico/logo.ico">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
    <section class="vh-100">
        <div class="mask d-flex align-items-center h-100 bg-light">
            <div class="container h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                        <div class="card" style="border-radius: 15px;">
                            <div class="card-body p-5">
                                <h3 class="text-center mb-5">Registrar Usuario</h3>

                                <form action="registro.php" method="POST"> 
                                    <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Nombre/s</label>
                                    <input required type="text" name="nombre" id="nombre" class="form-control form-control-lg mb-2" >

                                    <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Apellido/s</label>
                                    <input required type="text" name="apellido" id="apellido" class="form-control form-control-lg mb-2">
                                    
                                    <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Nombre de Usuario</label>
                                    <input required type="text" name="usuario" id="usuario" class="form-control form-control-lg mb-2" placeholder="O apodo" />

                                    <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Correo electrónico</label>
                                    <input required type="email" placeholder="@ejemplo.com" name="correo" id="correo" class="form-control form-control-lg mb-2"/>

                                    <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Contraseña</label>
                                    <input required type="password" placeholder="Ej. Prohibido Olvidarla!" name="contrasena" id="contrasena" class="form-control form-control-lg mb-2" />
                                    
                                    <input class="btn btn-outline-success btn-lg gradient-custom-3 form-control mt-4" type="submit" value="Registrar">
                                </form>
                                <?php 

                                    if(!isset($_SESSION['user']) || $_SESSION['rol'] != 2){ 
                                        echo "
                                        <p class='text-center text-muted mt-5 mb-0'>Ya tienes una cuenta?
                                            <a href='../'class='fw-bold text-body'><u>Inicia Sesión</u></a>
                                        </p>
                                        ";
                                    } 

                                    if(isset($_POST['nombre'],$_POST['apellido'],$_POST['usuario'],$_POST['correo'],$_POST['contrasena'])) {

                                        if(!empty($_POST['nombre']) && !empty($_POST['apellido']) && !empty($_POST['usuario']) && !empty($_POST['correo']) && !empty($_POST['contrasena'])) {

                                            if(isset($_POST['rol'])){
                                                $rol = $_POST['rol'];
                                            } else {
                                                $rol = 1;
                                            }

                                            $name = $_POST['nombre'];
                                            $lastName = $_POST['apellido'];
                                            $user = $_POST['usuario'];
                                            $email = $_POST['correo'];
                                            $pass = sha1($_POST['contrasena']);

                                            require '../config/connections/db_connection.php';

                                            // Consultar si el correo (indice único) existe
                                            $valid_sql = "SELECT COUNT(*) AS count FROM usuarios WHERE correo = :correo";

                                            $valid_sql = $conn->prepare($valid_sql);
                                                        // Asocia marcador con variable
                                                                                        // Limpia el dato para consulta
                                            $valid_sql->bindParam(':correo', $email, PDO::PARAM_STR);
                                            $valid_sql->execute();

                                            // Guarda la cantidad de filas que contó con el Select 
                                            $users_count = $valid_sql->fetch(PDO::FETCH_ASSOC);

                                            if($users_count['count'] > 0){
                                                
                                                include '../includes/alerts/register_duplicated.php';

                                            } else {

                                                // Registrar un nuevo usuario ya validado
                                                $register_sql = "INSERT INTO usuarios (usuario, correo, contrasena, nombre, apellido, rol) VALUES (:usuario, :correo, :contrasena, :nombre, :apellido,  :rol)";
                                                
                                                // Preparar la consulta utilizando PDO con vinculación de parametrosx
                                                $register_sql = $conn->prepare($register_sql);

                                                // Vincular parametros con sus marcadores de posición
                                                $register_sql->bindParam(':usuario', $user, PDO::PARAM_STR);
                                                $register_sql->bindParam(':correo', $email, PDO::PARAM_STR);
                                                $register_sql->bindParam(':contrasena', $pass, PDO::PARAM_STR);
                                                $register_sql->bindParam(':nombre', $name, PDO::PARAM_STR);
                                                $register_sql->bindParam(':apellido', $lastName, PDO::PARAM_STR);
                                                $register_sql->bindParam(':rol', $rol, PDO::PARAM_STR);
                                                $register_sql->execute();
                                                
                                                include '../includes/alerts/success/user_success.php';
                                            }
                                                    
                                        } else {
                                            include '../includes/alerts/form_empty.php';
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="assets/css/style.css">
</body>
</html>