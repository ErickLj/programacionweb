<?php require '../../config/connections/session_validate.php'; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Materias</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
        <link rel="stylesheet" href="../../assets/css/style.css">
        <link rel="icon" href="../../assets/ico/logo.ico">
    </head>
    <body>
        <?php include '../../includes/components/header.php'; ?>
        <div class="container-fluid">
            <div class="row">
                <?php include '../../includes/components/sidebar.php'; ?>
                    <div class="col-lg-10 col-sm-12 col-md-9 screen-height p-0 scrollbar-y bg-light ">
                        <div class="container py-5 h-100">
                            <div class="row d-flex justify-content-center align-items-center h-100">
                            <div class="col-md-12 col-xl-10 ">
                                <div class="card mb-5">
                                    <div class="card-body p-4 table-responsive ">
                                        <div class="text-center pt-3 pb-2 ">
                                        
                                        <h2 class="my-4">Lista de Materias</h2>
                                        <?php
                                        if(isset($_SESSION['msg'])){
                                            switch($_SESSION['msg']) {
                                                case 1:
                                                    include '../../includes/alerts/success/update_class_success.php';
                                                    unset($_SESSION['msg']);
                                                    break;
                                                case 2:
                                                    include '../../includes/alerts/wrong_credits.php';
                                                    unset($_SESSION['msg']);
                                                    break;

                                                case 3:
                                                    include '../../includes/alerts/wrong_prefix.php';
                                                    unset($_SESSION['msg']);
                                                    break;

                                                case 4:
                                                    include '../../includes/alerts/form_empty.php';
                                                    unset($_SESSION['msg']);
                                                    break;
                                                case 5:
                                                    include '../../includes/alerts/success/delete_class_success.php';
                                                    unset($_SESSION['msg']);
                                                    break;
                                                case 6:
                                                    include '../../includes/alerts/success/create_class_success.php';
                                                    unset($_SESSION['msg']);
                                                    break;
                                                case 10:
                                                    include '../../includes/alerts/success/update_user_success.php';
                                                    unset($_SESSION['msg']);
                                                    break;
                                                default:
                                                break;
                                            }
                                        } 
                                        ?>
                                        </div>
                                        <h5>Registra una nueva materia</h5>
                                        <form action="../../config/controlers/materia.insertar.php" method="POST">
                                            <div class="input-group mb-3">
                                                <span class="input-group-text">Prefijo</span>
                                                <input required type="text" name="prefijo" id="prefijo" class="form-control" placeholder="Ej. ABC">
                                                <span class="input-group-text">Créditos</span>
                                                <input required type="number" name="creditos" id="creditos" class="form-control" placeholder="Ej. 8">
                                            </div>
                                            <div class="input-group mb-3">
                                                <input required type="text" name="nombre" id="nombre" class="form-control" placeholder="Materia"/>
                                                <button type="submit" class="btn btn-secondary btn-block gradient-custom-2">Guardar</button>
                                            </div>
                                        </form>

                                        <?php
                                            require '../../config/connections/db_connection.php';
                                            if($_SESSION['rol']==2) {
                                                $class_list_sql = "SELECT materias.*, usuarios.usuario AS usuario_usuario FROM materias JOIN usuarios ON materias.usuario = usuarios.id  ORDER BY materias.id DESC";
                                            } else {
                                                $class_list_sql = "SELECT materias.*, usuarios.usuario AS usuario_usuario FROM materias JOIN usuarios ON materias.usuario = usuarios.id WHERE materias.usuario = ".$_SESSION['id']." ORDER BY materias.id DESC";
                                            }

                                            $class_list_sql = $conn->query($class_list_sql);

                                            if($class_list_sql->rowCount() > 0) {

                                        ?>

                                        <table class="table text-white mb-0 table-responsive">
                                            <thead>
                                                <tr>
                                                <th scope="col" class="text-center">Prefijo</th>
                                                <th scope="col" class="text-center">Materia o Asignatura</th>
                                                <th scope="col" class="text-center">Créditos otorgados</th>
                                                <?php if($_SESSION['rol']==2) {
                                                echo "<th scope='col' class='text-center'>Usuario asociado</th>"; 
                                                } ?>
                                                
                                                <th scope="col">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($class_list_sql as $tmp) { ?> 
                                                <tr class="fw-normal">
                                                    <th class="align-middle text-center">
                                                        <span class="ms-2"><?= $tmp['prefijo']?></span>
                                                    </th>
                                                    <td class="align-middle">
                                                        <span><?= $tmp['nombre']?></span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <h6 class="mb-0"><span class="badge bg-danger gradient-custom-2"><?= $tmp['creditos']?> </span></h6>
                                                    </td>
                                                    <?php if($_SESSION['rol']==2) {
                                                        echo "
                                                        <td class='align-middle'>
                                                            <h6 class='mb-0'><span class='badge bg-danger'>". $tmp['usuario_usuario'] ."</span></h6>
                                                        </td>
                                                        ";
                                                    } ?>
                                                    <td class="align-middle">
                                                        
                                                        <a class="btn btn-outline-success m-1" href="materia_editar.php?id=<?=$tmp['id']?>"><i class="bi bi-pencil-fill"></i></a>
                                                        <?php include 'materia_eliminar.php';?>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        </div>
                                        <?php
                                            } else {
                                                echo "No hay materias registradas!";
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                 
            </div>
        </div>
        <?php include '../../includes/components/footer.php'; ?>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    </body>
</html>