<button type="button" class="btn btn-outline-danger m-1" data-bs-toggle="modal" data-bs-target="#modalDelete<?=$tmp['id']?>">
<i class="bi bi-trash-fill"></i>
</button> 

<!--Modal Borrar <?php //  "(Su id se forma con modalDelete + el ID de la tarea en la bd)" ?> -->
<div class="modal fade" id="modalDelete<?=$tmp['id']?>" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel">Seguro que deseas Eliminar?</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <?= $tmp['nombre']?>
            </div>
            <div class="modal-footer">
                <a type="button" class="btn btn-danger"  href="../../config/controlers/materia.eliminar.php?id=<?=$tmp['id']; ?>">Eliminar</a>
            </div>
        </div>
    </div>
</div>  
