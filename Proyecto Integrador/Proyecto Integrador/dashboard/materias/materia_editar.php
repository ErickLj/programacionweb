<?php 
    require '../../config/connections/session_validate.php'; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Materia</title>
    <link rel="stylesheet" href="../../assets/css/style.css">
    <link rel="icon" href="../../assets/ico/logo.ico">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<?php include '../../includes/components/header.php'; ?>
        <div class="container-fluid">
            <div class="row">
                <?php include '../../includes/components/sidebar.php'; ?>
                    <div class="col-lg-10 col-sm-12 screen-height px-0">
                            <div class="align-items-center h-100 bg-light">
                                <div class="container h-100">
                                    <div class="row d-flex justify-content-center align-items-center h-100">
                                        <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                                            <div class="card" style="border-radius: 15px;">
                                                <div class="card-body p-5">
                                                    <h3 class="text-center mb-5">Editar materia</h3>

                                                    <?php
                                                        $class_id = $_GET['id'];
                                                        require '../../config/connections/db_connection.php';
                                                                                                                                                    
                                                        $class_edit_sql = "SELECT * FROM materias WHERE id = :id";
                                                        $class_edit_sql = $conn->prepare($class_edit_sql);
                                                        $class_edit_sql->bindParam(':id', $class_id);
                                                        $class_edit_sql->execute();
                                                        if($class_edit_sql->rowCount() > 0) {

                                                            $class = $class_edit_sql->fetch(PDO::FETCH_ASSOC);
                                                        ?>
                                                            <form action="../../config/controlers/materia.editar.php" method="POST"> 
                                                                <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Título</label>
                                                                <input required type="text" name="nombre" id="nombre" class="form-control form-control-lg mb-3" value="<?=$class['nombre']?>"/>
                                                            
                                                                <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Prefijo</label>
                                                                <input required placeholder="Ej. ABC, PTI..."type="text" name="prefijo" id="prefijo" class="form-control form-control-lg mb-3" value="<?=$class['prefijo']?>"/>
                                                                
                                                                <label class="form-label col-lg-6 col-12 mb-1 fs-5 text-success">Créditos</label>
                                                                <input required type="number" name="creditos" id="creditos" class="form-control form-control-lg mb-3" value="<?=$class['creditos']?>"/>
                                                                <input required type="hidden" id="id" name="id" value="<?=$class['id']?>">
                                                                
                                                                <div class="d-flex form-control">
                                                                    <input class="btn btn-outline-dark btn-lg mt-2" type="submit" value="Guardar Cambios" >
                                                                </div>
                                                            </form>
                                                                
                                                            <?php
                                                            
                                                        } else {
                                                            echo "Esa materia no fue encontrada! ";
                                                        }
                                                        if(isset($_SESSION['msg'])){
                                                            
                                                            switch($_SESSION['msg']) {
                
                                                                case 2:
                                                                    include '../../includes/alerts/wrong_credits.php';
                                                                    unset($_SESSION['msg']);
                                                                    break;
                
                                                                case 3:
                                                                    include '../../includes/alerts/wrong_prefix.php';
                                                                    unset($_SESSION['msg']);
                                                                    break;
                
                                                                case 4:
                                                                    include '../../includes/alerts/form_empty.php';
                                                                    unset($_SESSION['msg']);
                                                                    break;
                                                                case 10:
                                                                    include '../../includes/alerts/success/update_user_success.php';
                                                                    unset($_SESSION['msg']);
                                                                    break;
                                                                default:
                                                                    unset($_SESSION['msg']);
                                                                break;
                                                            }
                                                        }

                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include '../../includes/components/footer.php'; ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="assets/css/style.css">
</body>
</html>