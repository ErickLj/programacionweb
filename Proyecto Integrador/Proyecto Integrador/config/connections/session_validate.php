<?php
    session_start();
    if(!isLogged()){
        header("Location: ../");
        exit;
    }

    function isLogged()
    {return $_SESSION['user'];}

?>