<?php
    session_start();
    if(!isset($_SESSION['user']) || $_SESSION['rol'] != 2){
        // Función de js que regresa a la ventana anterior
        echo '<script>window.history.go(-1);</script>';
        exit;
    }

?>