<?php
    
    $host = 'localhost'; //127.0.0.1
    $dbname = 'db_taskerp';
    $dbuser = 'root';
    $dbpassword = '';


    try {
        
        $conn = new PDO("mysql:host=$host; dbname=$dbname", $dbuser, $dbpassword);

    } catch(PDOException $e){
            echo "<h1>Existe un error en la conexión de la BD!: " . $e->getMessage() . "</h1>";
    }

?>