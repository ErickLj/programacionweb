
<?php
    require '../../config/connections/admin_validate.php'; 
    
    if(isset($_POST['nombre'],$_POST['apellido'],$_POST['usuario'],$_POST['correo'],$_POST['contrasena'],$_POST['rol'])) {

        if(!empty($_POST['nombre']) && !empty($_POST['apellido']) && !empty($_POST['usuario']) && !empty($_POST['correo']) && !empty($_POST['contrasena'])&& !empty($_POST['rol'])) {

            $name = $_POST['nombre'];
            $lastName = $_POST['apellido'];
            $user = $_POST['usuario'];
            $email = $_POST['correo'];
            $pass = sha1($_POST['contrasena']);
            $rol = $_POST['rol'];
            
            require '../../config/connections/db_connection.php';

            // Consultar si el correo (indice único) existe
            $valid_sql = "SELECT COUNT(*) AS count FROM usuarios WHERE correo = :correo";

            $valid_sql = $conn->prepare($valid_sql);
                        // Asocia marcador con variable
                                                        // Limpia el dato para consulta
            $valid_sql->bindParam(':correo', $email, PDO::PARAM_STR);
            $valid_sql->execute();

            // Guarda la cantidad de filas que contó con el Select 
            $users_count = $valid_sql->fetch(PDO::FETCH_ASSOC);

            if($users_count['count'] > 0){
                
                $_SESSION['msg'] = 1;
                echo '<script>window.history.go(-1);</script>';

            } else {

                // Registrar un nuevo usuario ya validado
                $register_sql = "INSERT INTO usuarios (usuario, correo, contrasena, nombre, apellido, rol) VALUES (:usuario, :correo, :contrasena, :nombre, :apellido,  :rol)";
                
                // Preparar la consulta utilizando PDO con vinculación de parametrosx
                $register_sql = $conn->prepare($register_sql);

                // Vincular parametros con sus marcadores de posición
                $register_sql->bindParam(':usuario', $user, PDO::PARAM_STR);
                $register_sql->bindParam(':correo', $email, PDO::PARAM_STR);
                $register_sql->bindParam(':contrasena', $pass, PDO::PARAM_STR);
                $register_sql->bindParam(':nombre', $name, PDO::PARAM_STR);
                $register_sql->bindParam(':apellido', $lastName, PDO::PARAM_STR);
                $register_sql->bindParam(':rol', $rol, PDO::PARAM_STR);
                $register_sql->execute();
                
                $_SESSION['msg'] = 2;
                echo '<script>window.history.go(-1);</script>';
            }
                    
        } else {
            $_SESSION['msg'] = 4;
        }
        echo '<script>window.history.go(-1);</script>';
    }

?>