<?php
    session_start();
    if(!isset($_SESSION['user'])){
        // Función de js que regresa a la ventana anterior
        echo '<script>window.history.go(-1);</script>';
        exit;
    }
    
    if(isset($_POST['titulo'],$_POST['descripcion'],$_POST['prioridad'],$_POST['materia'],$_POST['fecha_vencimiento'],$_POST['estado'],$_POST['id'])) {

        if(!empty($_POST['titulo'])     && !empty($_POST['descripcion']) && !empty($_POST['prioridad']) 
                                        && !empty($_POST['materia']) && !empty($_POST['fecha_vencimiento']) 
                                        && !empty($_POST['estado'])&& !empty($_POST['id'])) {
            
                    $titl = $_POST['titulo'];
                    $desc = $_POST['descripcion'];
                    $prio = $_POST['prioridad'];
                    $matr = $_POST['materia'];
                    $expd = $_POST['fecha_vencimiento'];
                    $stat = $_POST['estado'];
                    $id = $_POST['id'];
                    require '../../config/connections/db_connection.php';


                    // Registrar un nuevo usuario ya validado
                    $task_update_sql = "UPDATE tareas SET titulo = :titulo, fecha_vencimiento = :fecha_vencimiento, 
                    descripcion = :descripcion, estado = :estado, prioridad = :prioridad, materia = :materia
                    WHERE id = :id";
                    
                    // Preparar la consulta utilizando PDO con vinculación de parametrosx
                    $task_update_sql = $conn->prepare($task_update_sql);

                    // Vincular parametros con sus marcadores de posición
                    $task_update_sql->bindParam(':titulo', $titl, PDO::PARAM_STR);
                    $task_update_sql->bindParam(':fecha_vencimiento', $expd, PDO::PARAM_STR);
                    $task_update_sql->bindParam(':descripcion', $desc, PDO::PARAM_STR);
                    $task_update_sql->bindParam(':estado', $stat, PDO::PARAM_STR);
                    $task_update_sql->bindParam(':prioridad', $prio, PDO::PARAM_STR);
                    $task_update_sql->bindParam(':materia', $matr, PDO::PARAM_STR);
                    $task_update_sql->bindParam(':id', $id, PDO::PARAM_STR);

                    $task_update_sql->execute();

                    $_SESSION['msg'] = 3;
                    echo '<script>window.history.go(-2);</script>';
        } else {

            $_SESSION['msg'] = 2;
            echo '<script>window.history.go(-1);</script>';
            
        }
        echo '<script>window.history.go(-1);</script>';
    } else {
        $_SESSION['msg'] = 2;
        echo '<script>window.history.go(-1);</script>';
    }
?>