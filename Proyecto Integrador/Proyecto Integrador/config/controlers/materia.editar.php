<?php
    session_start();
    if(!isset($_SESSION['user'])){
        // Función de js que regresa a la ventana anterior
        echo '<script>window.history.go(-1);</script>';
        exit;
    }

    if(isset($_POST['nombre'],$_POST['prefijo'],$_POST['creditos'],$_POST['id'])) {

        if(!empty($_POST['nombre']) && !empty($_POST['prefijo']) && !empty($_POST['creditos'])&& !empty($_POST['id'])) {

            if(strlen($_POST['prefijo'])<=3) {     
                
                if($_POST['creditos'] > 0 && is_numeric($_POST['creditos']) ) {
                    $name = $_POST['nombre'];
                    $pref = $_POST['prefijo'];
                    $cred = $_POST['creditos'];
                    $id = $_POST['id'];

                    require '../connections/db_connection.php';

                    // Registrar un nuevo usuario ya validado
                    $update_sql = "UPDATE materias SET prefijo = :prefijo, nombre = :nombre, creditos = :creditos WHERE materias.id = :id";
                    
                    // Preparar la consulta utilizando PDO con vinculación de parametrosx
                    $update_sql = $conn->prepare($update_sql);

                    // Vincular parametros con sus marcadores de posición
                    $update_sql->bindParam(':prefijo', $pref, PDO::PARAM_STR);
                    $update_sql->bindParam(':nombre', $name, PDO::PARAM_STR);
                    $update_sql->bindParam(':creditos', $cred, PDO::PARAM_STR);
                    $update_sql->bindParam(':id', $id, PDO::PARAM_STR);
                    $update_sql->execute();
                    
                    $_SESSION['msg'] = 1;
                    echo '<script>window.history.go(-2);</script>';
                    
                } else {
                    $_SESSION['msg'] = 2;
                    echo '<script>window.history.go(-1);</script>';
                }
            } else {
                $_SESSION['msg'] = 3;
                echo '<script>window.history.go(-1);</script>';
            }
        } else {
            $_SESSION['msg'] = 4;
            echo '<script>window.history.go(-1);</script>';
        }
    
    }
?>