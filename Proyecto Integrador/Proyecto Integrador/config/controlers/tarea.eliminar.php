<?php
// Validate un administrador, hacerlo función única
    session_start();
    if(!isset($_SESSION['user'])){
        // Función de js que regresa a la ventana anterior
        echo '<script>window.history.go(-1);</script>';
        exit;
    }

    if(isset($_GET['id']) && !empty($_GET['id'])){

        require '../../config/connections/db_connection.php';

        $task_id = $_GET['id'];

        $task_delete_sql = "DELETE FROM tareas WHERE id = :id";
        $valid_sql = $conn->prepare($task_delete_sql);
        $valid_sql->bindParam(':id', $task_id);
        $valid_sql->execute();

        // Referemcia para volver a la pagina post borrar
        $_SESSION['msg'] = 1;
        echo '<script>window.history.go(-1);</script>';

        //Aviso de eliminación

    } else {
        echo '<script>window.history.go(-1);</script>';
    }
    

    
?>