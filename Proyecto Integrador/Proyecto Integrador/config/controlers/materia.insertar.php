<?php
    session_start();
    if(!isset($_SESSION['user'])){
        // Función de js que regresa a la ventana anterior
        echo '<script>window.history.go(-1);</script>';
        exit;
    }
    
    if(isset($_POST['prefijo'],$_POST['nombre'],$_POST['creditos'])) {

        if(!empty($_POST['prefijo']) && !empty($_POST['nombre']) && !empty($_POST['creditos'] )) {
            
            if(strlen($_POST['prefijo'])<=3) { 

                if($_POST['creditos'] > 0 && is_numeric($_POST['creditos']) ) {

                    $name = $_POST['nombre'];
                    $pref = $_POST['prefijo'];
                    $cred = $_POST['creditos'];
                    $user = $_SESSION['id'];
                    require '../../config/connections/db_connection.php';


                    // Registrar un nuevo usuario ya validado
                    $class_sql = "INSERT INTO materias (prefijo, nombre, creditos,usuario) VALUES (:prefijo, :nombre, :creditos, :usuario)";
                    
                    // Preparar la consulta utilizando PDO con vinculación de parametrosx
                    $class_sql = $conn->prepare($class_sql);

                    // Vincular parametros con sus marcadores de posición
                    $class_sql->bindParam(':nombre', $name, PDO::PARAM_STR);
                    $class_sql->bindParam(':prefijo', $pref, PDO::PARAM_STR);
                    $class_sql->bindParam(':creditos', $cred, PDO::PARAM_STR);
                    $class_sql->bindParam(':usuario', $user, PDO::PARAM_STR);

                    $class_sql->execute();

                    $_SESSION['msg'] = 6;
                    
                } else {
                    $_SESSION['msg'] = 2;
                }
            } else {
                $_SESSION['msg'] = 3;
            }
        } else {
            $_SESSION['msg'] = 4;
            
        }
        echo '<script>window.history.go(-1);</script>';
    }
?>