<?php

    session_start();
    if(!isset($_SESSION['user'])){

        echo '<script>window.history.go(-1);</script>';
        exit;
    }

    if(isset($_GET['id']) && !empty($_GET['id'])){

        require '../../config/connections/db_connection.php';

        $task_id = $_GET['id'];
    
        $state_update_sql = "UPDATE tareas SET estado = 3 WHERE id = :id";
        $state_update_sql = $conn->prepare($state_update_sql);
        $state_update_sql->bindParam(':id', $task_id);
        $state_update_sql->execute();

        $_SESSION['msg'] = 1;
        echo '<script>window.history.go(-1);</script>';

    } else {
        echo '<script>window.history.go(-1);</script>';
    }
    

    
?>