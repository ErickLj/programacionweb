<?php
    require '../../config/connections/session_validate.php';

    if(isset($_POST['nombre'],$_POST['apellido'],$_POST['usuario'],$_POST['correo'],$_POST['rol'],$_POST['id'])) {

        if( !empty($_POST['nombre']) && !empty($_POST['apellido']) && !empty($_POST['usuario']) &&
            !empty($_POST['correo']) && !empty($_POST['rol']) && !empty($_POST['id'])) {

            $user = $_POST['usuario'];
            $email = $_POST['correo'];
            $name = $_POST['nombre'];
            $lastName = $_POST['apellido'];
            $rol = $_POST['rol'];
            $id = $_POST['id'];

            require '../connections/db_connection.php';

            $valid_sql = "SELECT COUNT(*) AS count FROM usuarios WHERE correo = :correo AND id != :id";
            $valid_sql = $conn->prepare($valid_sql);

            $valid_sql->bindParam(':correo', $email, PDO::PARAM_STR);
            $valid_sql->bindParam(':id', $id, PDO::PARAM_STR);
            $valid_sql->execute();

            $users_count = $valid_sql->fetch(PDO::FETCH_ASSOC);
            echo $users_count['count'];


                //REVISAR LA CONDICION, VALIDAR SOLO CON EL CORREO PARA EVITAR COSAS, DEJAR EL CAMPO DE USUARIO SOLO COMO UN NICKNAME BONITO



            if($users_count['count'] > 0 ) {
                
                $_SESSION['msg'] = 1; //MENSAJE DE, EL USUARIO QUE EDITAS NO EXISTE SABE PQ
                echo '<script>window.history.go(-1);</script>';

            } else {
                // Registrar un nuevo usuario ya validado
                $update_sql = "UPDATE usuarios SET usuario = :usuario, correo = :correo, nombre = :nombre, apellido = :apellido, rol = :rol WHERE id = :id";
                
                // Preparar la consulta utilizando PDO con vinculación de parametrosx
                $update_sql = $conn->prepare($update_sql);

                // Vincular parametros con sus marcadores de posición
                $update_sql->bindParam(':usuario', $user, PDO::PARAM_STR);
                $update_sql->bindParam(':correo', $email, PDO::PARAM_STR);
                $update_sql->bindParam(':nombre', $name, PDO::PARAM_STR);
                $update_sql->bindParam(':apellido', $lastName, PDO::PARAM_STR);
                $update_sql->bindParam(':rol', $rol, PDO::PARAM_STR);
                $update_sql->bindParam(':id', $id, PDO::PARAM_STR);
                $update_sql->execute();
                
                if($_POST['id'] == $_SESSION['id']){
                    require '../connections/refresh_session.php';
                }

                $_SESSION['msg'] = 3;
                echo '<script>window.history.go(-2);</script>';
            } 

        } else {
            $_SESSION['msg'] = 2;
            echo '<script>window.history.go(-1);</script>';
        }
        
    }
?>