
<?php
// Validate un administrador, hacerlo función única
    session_start();
    if(!isset($_SESSION['user'])|| $_SESSION['rol'] != 2){
        // Función de js que regresa a la ventana anterior
        echo '<script>window.history.go(-1);</script>';
        exit;
    }

    if(isset($_GET['id']) && !empty($_GET['id'])){

        require '../connections/db_connection.php';
        $class_id = $_GET['id'];

        $class_delete_sql = "DELETE FROM usuarios WHERE id = :id";
        $class_delete_sql = $conn->prepare($class_delete_sql);
        $class_delete_sql->bindParam(':id', $class_id);
        $class_delete_sql->execute();

        if($_GET['id'] == $_SESSION['id']) {
            require 'salir.php';
        }
        $_SESSION['msg'] = 5;
        echo '<script>window.history.go(-1);</script>';

        //Aviso de eliminación

    } else {
        echo '<script>window.history.go(-1);</script>';
    }
    

    
?>