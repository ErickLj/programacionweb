<?php
    
    function validateRol($rol) {
        switch($rol) {
            case 1: 
                return "Estudiante";
                break;
            case 2:
                return "Administrador";
                break;
        }
    }

    function validateEstado($estado) {
        switch($estado) {
            case 1:
                return "Administrador";
                break;
            case 1: 
                return "Estudiante";
                break;
        }
    }

    function isMe($id) {
        if($id == $_SESSION['id']){
            return "gradient-custom-1";
        } else {
            return "gradient-custom-3";
        }
    }

    function validatePrefix($prefix){
        if(strlen($prefix)==3) {
            return true;
        } else {
            return false;
        }
    }

    function cardColor($value) {
        switch($value){
            case 1:
                return "danger";
                break;
            case 2: 
                return "warning";
                break;
            case 3:
                return "success";
                break;
            default:
                return "info";
            break;
        }
    }

    function dueColor($value){

        $diferencia = (strtotime($value) - strtotime("now")) / (60 * 60 );

        if($diferencia <= 24){

            return "danger";

        } elseif($diferencia <= 72){

            return "warning";

        } else {

            return "success";
        }
    }
    
?>