<?php
    session_start();
    if (isset($_SESSION['user']) && isset($_SESSION['rol'])) {
        header("Location: dashboard/");
        exit;
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login|PHP</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="icon" href="assets/ico/logo.ico">
    </head>
    <body>  
        <section class="vh-100 bg-lightgray">
            <div class="container py-5 h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-xl-10">
                        <div class="card rounded-3 text-black">
                            <div class="row g-0">
                                <div class="col-lg-6 d-flex align-items-center gradient-custom-2">
                                    <div class="text-white px-3 py-4 p-md-5 mx-md-4">
                                        <h4 class="mb-4">Tus tareas en un solo lugar</h4>
                                        <p class="small mb-0">Mantén todas tus actividades y pendientes organizados de una forma 
                                            accesible y fácil de utilizar para que nunca más te olvides de un algo por hacer.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card-body p-md-5 mx-md-4">
                                        <div class="text-center">
                                            <h4 class="mt-1 mb-5 pb-1">Bienvenido a TASKERP</h4>
                                        </div>
                                        <form action="index.php" method="POST">
                                            <p>Inicia Sesión con tu cuenta</p>
                                            <div class="form-outline mb-4">
                                                <input required type="email" name="correo" id="correo" class="form-control" placeholder="Correo electrónico" required/>
                                            </div>
                                            <div class="form-outline mb-4">
                                                <input required type="password" name="contrasena" id="contrasena" class="form-control" placeholder="Contraseña" required/>
                                            </div>
                                            <div class="d-grid gap-2 my-3">
                                                <input type="submit" class="btn btn-primary btn-block fa-lg gradient-custom-2 mb-3 gradient-custom-2" value="Iniciar Sesión" /> 
                                            </div>
                                        </form>

                                        <div class="d-flex align-items-center justify-content-center pb-4">
                                            <a class="text-muted" href="#!">Olvidaste tu contraseña?</a>    
                                        </div>
                                        <div class="d-flex align-items-center justify-content-center pb-4">
                                            <p class="mb-0 me-2">Aún no tienes una cuenta?</p>
                                            <a href="dashboard/registro.php">
                                                <button class="btn btn-outline-success ">Regístrate</button>
                                            </a>
                                        </div>
                                        <?php
                                            if(isset($_POST['correo'],$_POST['contrasena'])) {
                                                
                                                if(!empty($_POST['correo']) && !empty($_POST['contrasena'])) {

                                                    require 'config/connections/db_connection.php';

                                                    $email = $_POST['correo'];
                                                    $password = sha1($_POST['contrasena']);

                                                    $query = "SELECT * FROM usuarios WHERE correo = :email AND contrasena =  :password"; // ':username y :password' Son alias que se referenciarán mas 
                                                                                                                                            // abajo. Aún no reciben parametros, los recibirá adelante.
                                                    $resultado = $conn->prepare($query);

                                                    $resultado->bindParam(':email',  $email, PDO::PARAM_STR); // El PDO permite limpiar el valor ingresado interpretado de otra forma
                                                    $resultado->bindParam(':password',  $password, PDO::PARAM_STR);
                                                    $resultado->execute();

                                                    if($resultado->rowCount() > 0 ) { // Valida la cantidad de filas devueltas por la consulta, si sale mas de una ingresa

                                                        $user = $resultado->fetch(PDO::FETCH_ASSOC);

                                                        $_SESSION['user'] = $user['usuario']; 
                                                        $_SESSION['name'] = $user['nombre'];
                                                        $_SESSION['id'] = $user['id']; 
                                                        $_SESSION['rol'] = $user['rol']; 
                                                        
                                                        header("Location: dashboard/index.php");

                                                    } else {
                                                        include 'includes/alerts/login_wrong_credentials.php';
                                                    }
                                                } else {
                                                    include 'includes/alerts/form_empty.php';
                                                }
                                            } 
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="assets/css/style.css">
    </body>
</html>