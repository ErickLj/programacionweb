<?php
    ob_start();
    session_start();

    // Validamos si fue un cierre de sesión
    if (isset($_SESSION['user']) && isset($_SESSION['pass'])) {
        session_destroy();
        header("Location: index.php");
        exit;
    }

    // Validamos si fue un cierre de cookies
    if (isset($_COOKIE['user']) && isset($_COOKIE['pass']) && isset($_COOKIE['color'])) {
        //Reajustamos el tiempo de vida de las cookies para que finalice
        setcookie('user', '', time() - 3600);
        setcookie('pass', '', time() - 3600);
        setcookie('color', '', time() - 3600);
        header("Location: index.php");
        exit;
    }


    
?>