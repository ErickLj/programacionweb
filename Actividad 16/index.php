<?php
    ob_start();
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login Sesión y Cookies </title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    </head>
<body>
    <nav class="navbar navbar-dark bg-dark sticky-top p-3 text-center">
        <div class="container-fluid">
            <h5 class="navbar-brand"><a href="../index.html"><i class="bi bi-folder"></i></i> Inicio</a> / Login</h5>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-end text-bg-dark" tabindex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasDarkNavbarLabel">Programación WEB</h5>
                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Volver a Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="../index.html">Menú Principal</a>
                        </li>
                    </ul>
                    <div class="d-grid gap-2">
                        <a class="btn btn-outline-light" href="https://gitlab.com/ErickLj" role="button" target="_blank"><i class="bi bi-gitlab"></i> @ErickLj</a>
                        <a class="btn btn-outline-info" href="linkedin.com/in/erick-arturo-lópez-jáuregui-568176227" role="button" target="_blank"><i class="bi bi-linkedin"></i> LinkedIn</a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <section class="w-50 mx-auto p-3"><br><br>
        <form action="index.php" method="POST">
            <div class="row mb-3">
                <div class="input-group mb-3">
                    <input type="email" class="form-control" name="usuario" id="usuario" placeholder="Usuario" aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <span class="input-group-text" id="basic-addon2">@ejemplo.com</span>
                </div>
            </div>
            <div class="row mb-3">
                <label for="contrasena" class="col-sm-2 col-form-label">Contraseña</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" name="contrasena" id="contrasena">
                </div>
            </div>
            <div class="d-grid gap-3 col-2 mx-auto">
                <input type="color" name="color" id="color" class="form-control ">
            </div>
            <div class="d-grid gap-2 my-3">
                <input type="submit" class="btn btn-secondary" value="Ingresar Sesión" name="login_session">
                <input type="submit" class="btn btn-info" value="Ingresar Cookies" name="login_cookies">
            </div>
        </form>
    </section>

    <?php

        // Validar primero si ya existen sesiones para que directo se mande al usuario a
        // la página sin pasar de nuevo por el login
        if (isset($_SESSION['user']) && isset($_SESSION['pass'])) {
            header("Location: session.php");
            exit;
        }

        if (isset($_COOKIE['user']) && isset($_COOKIE['pass']) && isset($_COOKIE['color'])) {
            header("Location: cookies.php");
            exit;
        }

        // Usuario estático
        $user = "erick.lopez@gmail.com";
        $pass = "1234";

        // Validar valores ingresados que existan
        if(isset($_POST['usuario'],$_POST['contrasena'],$_POST['color'])) {

            // Validar valores ingresados no estén vacíos
            if(!empty($_POST['usuario']) && !empty($_POST['contrasena'] && !empty($_POST['color']))) {

                // Validar que credenciales ingresadas coincidas con el estático
                if($user == $_POST['usuario'] && $pass == $_POST['contrasena']) {
                    
                    // Validar que tipo de login se realizó (Sesión o Cookie)
                    if(isset($_POST['login_session'])){
                        

                        $_SESSION['user'] = $_POST['usuario'];
                        $_SESSION['pass'] = $_POST['contrasena'];
                        $_SESSION['color'] = $_POST['color'];
                        $_SESSION['count'] =  0 ;
                        header("Location: session.php");

                    } else {
                        setcookie('user', $_POST['usuario'], time() + 60);
                        setcookie('pass', $_POST['contrasena'], time() + 60);
                        setcookie('color', $_POST['color'], time() + 60);
                        setcookie('count', 1, time() + 20);
                        header("Location: cookies.php");
                    }
                    
                } else {
                    echo "
                    <div class='alert alert-danger d-grid gap-2 col-2 mx-auto text-center' role='alert'>
                            <h5> Usuario o contraseña <strong>Incorrectos!</strong></h5>
                        </div>
                    ";
                }
            } else {
                echo "
                    <div class='alert alert-warning d-grid gap-2 col-2 mx-auto text-center' role='alert'>
                        <h5> No dejes campos <strong> Vacíos! </strong></h5>
                    </div>
                ";

            } 
        } 
    ?>

</body>

<footer class="bg-dark text-white pt-5  mt-3 text-center fixed-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <h5>Contacto</h5>
                    <ul class="list-unstyled">
                        <li>Tgo. en Desarrollo de Software: Erick López Jáuregui</li>
                        <li>Correo: erick2001.lopez@gmail.com</li>
                        <li>Teléfono: +52 33 1228 68 07</li>
                    </ul>
                </div>
                <div class="col-md-3 mb-3">
                    <h5>Navegación</h5>
                    <ul class="list-unstyled">
                        <li><a href="#" class="text-white">Volver al Inicio</a></li>
                        <li><a href="../index.html" class="text-white">Menú principal</a></li>
                    </ul>
                </div>
                <div class="col-md-3 mb-3">
                    <h5>Sociales</h5>
                    <ul class="list-inline">
                        <li class="list-inline-item"><a href="https://gitlab.com/ErickLj" class="text-white" target="_blank"><i class="bi bi-gitlab"></i> Repositorio de GitLab</a></li>
                        <li class="list-inline-item"><a href="linkedin.com/in/erick-arturo-lópez-jáuregui-568176227" class="text-white" target="_blank"><i class="bi bi-linkedin"></i> LinkedIn</a></li>
                    </ul>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-12 text-center">
                    <p>Licenciatura en Tecnologías de la Información. UNIVERSIDAD DE GUADALAJARA</p>
                </div>
            </div>
        </div>
    </footer>
</html>