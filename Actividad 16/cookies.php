<?php
    if(isset($_COOKIE['user'],$_COOKIE['pass'],$_COOKIE['color'],$_COOKIE['count'])){

        // Incrementa el contador de visitas de las cookies
        setcookie('count', $_COOKIE['count'] + 1, time() + 60);
        
    } else {
        header("Location: index.php");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP|ISSET </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</head>
<body>
    <nav class="navbar navbar-dark bg-dark sticky-top p-3 text-center">
        <div class="container-fluid">
            <h5 class="navbar-brand"><a href="../index.html"><i class="bi bi-folder"></i></i> Inicio</a> / Login</h5>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-end text-bg-dark" tabindex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasDarkNavbarLabel">Programación WEB</h5>
                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Volver a Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="../index.html">Menú Principal</a>
                        </li>
                    </ul>
                    <div class="d-grid gap-2">
                        <a class="btn btn-outline-light" href="https://gitlab.com/ErickLj" role="button" target="_blank"><i class="bi bi-gitlab"></i> @ErickLj</a>
                        <a class="btn btn-outline-info" href="linkedin.com/in/erick-arturo-lópez-jáuregui-568176227" role="button" target="_blank"><i class="bi bi-linkedin"></i> LinkedIn</a>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <?php
        echo "
        <br><br><br><br>
        <div class='card text-center w-50 m-auto'>
            <div class='card-header'>
                <p style='color:{$_COOKIE['color']}'> Entraste por <strong> Cookies</strong>.</p>
            </div>
            <div class='card-body'>
                <h5 style='color:{$_COOKIE['color']}'>Bienvenido </h5>
                <p class='card-text'> <strong>{$_COOKIE['user']} </strong></p>
                <button type='button' class='btn btn-secondary position-relative'>
                Visitas
                <span class='position-absolute top-0 start-100 translate-middle badge rounded-pill' style='background-color:{$_COOKIE['color']}'>
                    {$_COOKIE['count']}
                    <span class='visually-hidden'>unread messages</span>
                </span>
            </button>
            </div>
            <div class='card-footer text-body-secondary'>
                <p><a href='salir.php'>Salir</a></p>
            </div>
        </div>
        ";
    ?>
</body>

<footer class="bg-dark text-white pt-5  mt-3 text-center fixed-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <h5>Contacto</h5>
                    <ul class="list-unstyled">
                        <li>Tgo. en Desarrollo de Software: Erick López Jáuregui</li>
                        <li>Correo: erick2001.lopez@gmail.com</li>
                        <li>Teléfono: +52 33 1228 68 07</li>
                    </ul>
                </div>
                <div class="col-md-3 mb-3">
                    <h5>Navegación</h5>
                    <ul class="list-unstyled">
                        <li><a href="#" class="text-white">Volver al Inicio</a></li>
                        <li><a href="../index.html" class="text-white">Menú principal</a></li>
                    </ul>
                </div>
                <div class="col-md-3 mb-3">
                    <h5>Sociales</h5>
                    <ul class="list-inline">
                        <li class="list-inline-item"><a href="https://gitlab.com/ErickLj" class="text-white" target="_blank"><i class="bi bi-gitlab"></i> Repositorio de GitLab</a></li>
                        <li class="list-inline-item"><a href="linkedin.com/in/erick-arturo-lópez-jáuregui-568176227" class="text-white" target="_blank"><i class="bi bi-linkedin"></i> LinkedIn</a></li>
                    </ul>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-12 text-center">
                    <p>Licenciatura en Tecnologías de la Información. UNIVERSIDAD DE GUADALAJARA</p>
                </div>
            </div>
        </div>
    </footer>
</html>